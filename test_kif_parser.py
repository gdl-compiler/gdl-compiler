#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import os
from traceback import print_exc

from kif_lexer import kif_lexer
from kif_parser import kif_parser
from kif import *

def test(src):
    print('test:', src)
    with open(src, 'r') as fp:
        prgm = fp.read()
        try:
            ret = kif_parser.parse(prgm, lexer=kif_lexer)
            print('\tOK')
        except Exception as err:
            print('\tERROR')
            #print(repr(err))
            #print_exc(file=sys.stdout)

    
if __name__ == '__main__':
    src_dir = sys.argv[1]
    for filename in os.listdir(src_dir):
        if filename.endswith('.kif'):
            test(src_dir + '/' + filename)
