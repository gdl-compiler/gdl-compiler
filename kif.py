#!/usr/bin/env python3
# -*- coding: utf-8 -*-


import sys


class GDL_Obj:
    pass


class Compound(GDL_Obj):
    def __new__(cls, lst):
        name, args = lst[0], lst[1:]
        if not args:
            print('\tWARNING(malformed term) : {}'.format(name), file=sys.stderr)
            return name
        return super().__new__(cls)

    def __init__(self, lst):
        self.name = lst[0]
        self.args = lst[1:]
        self.arity = len(self.args)

    def to_gdl(self):
        return self.name.to_gdl() + '(' + ','.join(e.to_gdl() for e in self.args) + ')'

    def __repr__(self):
        return self.name + '/' + str(self.arity)

class Not(Compound):
    def __init__(self, lst):
        super().__init__(lst)
        assert self.arity == 1

    def to_gdl(self):
        return '~' + self.args[0].to_gdl()

class Atom(GDL_Obj):
    pass

# si le symbole est une tête de règle, la sémantique est incorrecte 
# (c'est un foncteur d'arité zéro et non un simple symbole).
class Symbol(Atom, str):
    def __new__(cls, name):
        return super().__new__(cls, name)

    def __init__(self, name):
        self.name = name
        self.arity = 0  # hack ?

    def to_gdl(self):
        return self.name

class Integer(Atom, int):
    def __new__(cls, nb):
        return super().__new__(cls, nb)

    def __init__(self, nb):
        self.nb = nb

    def to_gdl(self):
        return repr(self.nb)

class Variable(Atom, str):
    def __new__(cls, name):
        return super().__new__(cls, name.capitalize())

    def __init__(self, varname):
        self.name = varname

    def to_gdl(self):
        return self.name.capitalize()


class Rule(GDL_Obj):
    def __new__(cls, compound):
        head, body = compound.args[0], compound.args[1:]
        if not body:
            print('\tWARNING(malformed rule) : {}'.format(head.to_gdl()), file=sys.stderr)
            return Fact(head)
        return super().__new__(cls)

    def __init__(self, compound):
        head, body = compound.args[0], compound.args[1:]
        self.head = head
        self.body = tuple(body)

    def to_gdl(self):
        return self.head.to_gdl() + ' :- \n\t' + ', \n\t'.join(c.to_gdl() for c in self.body) + '.'

    def __repr__(self):
        return self.head.name + '/' + str(self.head.arity) 

class Fact(GDL_Obj):
    def __init__(self, compound):
        self.head = compound

    def to_gdl(self):
        return self.head.to_gdl() + '.'

    def __repr__(self):
        return self.head.name + '/' + str(self.head.arity)
