#!/usr/bin/env python3
# -*- coding: utf-8 -*-


class Logic(tuple):
    cache = dict()
    def __new__(cls, args):
        t = cls, tuple(args)
        e = Logic.cache.get(t)
        if e is not None:
            return e
        r = super().__new__(cls, args)
        Logic.cache[t] = r
        return r
    def __eq__(self, other):
        return type(self) == type(other) and super().__eq__(self, other)
    def __hash__(self):
        return id(self)

class Not(Logic):
    def __init__(self, args):
        assert len(args) == 1
        self.term = args[0]
    def __repr__(self):
        return '~' + repr(self.term)
    def simplify(self):
        # add double negation rule
        if isinstance(self.term, Logic):
            return self.term.simplify()
        else:
            return self

class And(Logic):
    def __repr__(self):
        return '(' + ' & '.join(repr(c) for c in self) + ')'
    def simplify(self):
        if len(self) == 1:
            return self[0].simplify() if isinstance(self[0], Logic) else self[0]
        else:
            return And([nd.simplify() if isinstance(nd, Logic) else nd for nd in self])

class Or(Logic):
    def __repr__(self):
        return '(' + ' | '.join(repr(c) for c in self) + ')'
    def simplify(self):
        if len(self) == 1:
            return self[0].simplify() if isinstance(self[0], Logic) else self[0]
        else:
            return Or([nd.simplify() if isinstance(nd, Logic) else nd for nd in self])

