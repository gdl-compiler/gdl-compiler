#!/usr/bin/env python3
# -*- coding: utf-8 -*-


from pprint import pprint
from collections import defaultdict
from itertools import chain


from gdl_lexer import gdl_lexer
from gdl_parser import gdl_parser, Compound, Fact, Rule
from logic import *


def foo(filename):
    dct = {}
    for entry in bar(filename):
        if isinstance(entry, Fact):
            dct[repr(entry.head)] = None
        elif isinstance(entry, Rule):
            dct.setdefault(repr(entry.head), []).append(
                And([Not([repr(clause.term)]) if isinstance(clause, Not) else repr(clause)
                    for clause in entry.body]))
        else:
            raise RuntimeError('unexcepted entry !')
    # adding internal nodes ?
    return {k: Or(lst).simplify() if lst else None for k, lst in dct.items()}

def bar(filename):
    with open(filename, 'r') as fp:
        return gdl_parser.parse(fp.read(), lexer=gdl_lexer)


##def is_input(nd):
##    return any(nd.startswith(s) for s in ['true','does'])
##
##def is_output(nd):
##    return any(nd.startswith(s) for s in ['next','legal','terminal','goal'])

def gensym(nd):
    if isinstance(nd, Not):
        return 'not_' + str(id(nd))
    elif isinstance(nd, And):
        return 'and_' + str(id(nd))
    elif isinstance(nd, Or):
        return 'or_' + str(id(nd))
    else:
        return nd.replace(',','_').replace('(','_').replace(')','_')

##def gensym(nd):
##    if isinstance(nd, (Not, Or, And)):
##        return gensym(repr(nd))
##    else:
##        return nd.replace("'", '').replace(',','_').replace('(','K_').replace(')','_X').replace(' ', '').replace('~', 'NOT_').replace('&', '_AND_').replace('|', '_OR_')

#fp = open('minittc.py', 'w')

class PropNet:
    def __init__(self, dct, init):
        self.nodes = PropNet.classify(dct.keys())
        self._dct = dct
        self.tr_order = self.topological_sort(self.nodes['next'])
        self.ct_order = self.topological_sort(dct.keys() - self.nodes['next'])
        self.init = init
        self.vars = {}
        
        for nd in self.nodes['base']:
            self.vars[nd] = nd in init
            #fp.write('{} = {}\n'.format(gensym(nd), self.vars[nd]))
        for nd in self.nodes['input']:
            self.vars[nd] = False
            #fp.write('{} = {}\n'.format(gensym(nd), self.vars[nd]))
        self.compute(self.ct_order)

    @staticmethod
    def classify(nodes):
        dct = {k: set() for k in ['base', 'input', 'next', 'legal', 'terminal', 'goal', 'view']}
        for nd in nodes:
            if nd.startswith('true'):
                dct['base'].add(nd)
            elif nd.startswith('does'):
                dct['input'].add(nd)
            elif nd.startswith('next'):
                dct['next'].add(nd)
            elif nd.startswith('legal'):
                dct['legal'].add(nd)
            elif nd.startswith('goal'):
                dct['goal'].add(nd)
            elif nd.startswith('terminal'):
                dct['terminal'].add(nd)
            else:
                dct['view'].add(nd)
        return dct

    def topological_sort(self, seq):
        lst = []
        marking = {}

        def visit(n):
            if n is None:
                marking[n] = True
                return
            if marking.get(n) == False:
                raise RuntimeError('cycle on {}'.format(n))
            elif n not in marking:
                marking[n] = False
                if n in self._dct:
                    l = [self._dct[n]]
                elif isinstance(n, (Not, Or, And)):
                    l = iter(n)
                else:
                    raise RuntimeError('arg {} not found'.format(n))
                for m in l:
                    visit(m)
                marking[n] = True
                lst.append(n)
            
        for nd in seq:
            visit(nd)

        return lst

    def compute(self, order):
        dct = self.vars

        for i, nd in enumerate(order):
            # print(nd)
            if isinstance(nd, Not):
                dct[nd] = not dct[nd.term]
                #print(i, 'dct[{}] = not dct[{}] = {}'.format(nd, nd.term, dct[nd]))
                #fp.write('{} = not {}\n'.format(gensym(nd), gensym(nd.term)))
            elif isinstance(nd, And):
                assert len(nd) >= 2
                dct[nd] = all(dct[term] for term in nd)
                #print(i, 'dct[{}] = {} = {}'.format(nd, ' && '.join('dct[{}]'.format(term) for term in nd), dct[nd]))
                #fp.write('{} = {}\n'.format(gensym(nd), ' and '.join('{}'.format(gensym(term)) for term in nd)))
            elif isinstance(nd, Or):
                assert len(nd) >= 2
                dct[nd] = any(dct[term] for term in nd)
                #print(i, 'dct[{}] = {} = {}'.format(nd, ' || '.join('dct[{}]'.format(term) for term in nd), dct[nd]))
                #fp.write('{} = {}\n'.format(gensym(nd), ' or '.join('{}'.format(gensym(term)) for term in nd)))
            else:
                if nd in self.nodes['input'] or nd in self.nodes['base']:
                    continue
                key = self._dct[nd]
                dct[nd] = dct[key]
                #print(i, 'dct[{}] = {} = {}'.format(nd, key, dct[nd]))
                #fp.write('{} = {}\n'.format(gensym(nd), gensym(key)))
            # input('continuer ?')
                
    def next_state(self, inputs):
        # marking input
        for nd in self.nodes['input']:
            self.vars[nd] = nd in inputs
        # make transitions (mark base)
        self.make_transitions()
        # compute
        self.compute(self.ct_order)

    def make_transitions(self):
        dct = self.vars

        self.compute(self.tr_order)

        for nd in self.nodes['next']:
            key = self._dct[nd]
            dct[nd.replace('next', 'true')] = dct[key]

    def get_current_state(self, key):
        return {k for k, v in self.vars.items() if k in self.nodes[key] and v}

    def debug(self):
        for key in ['base', 'input', 'view', 'legal', 'goal', 'terminal']:
            print(key, ':', propnet.get_current_state(key))
        print(50 * '-')


def to_dot(dct, filename, print_transitions=True):
    visited = set()
    fp = open(filename, 'w')

    def rec_visit(nd):
        if nd in visited:
            return
        else:
            visited.add(nd)

        uid = gensym(nd)

        # add node
        if isinstance(nd, Or):
            fp.write(' "{}"[shape=ellipse, style= filled, fillcolor=grey, label="OR"];\n'.format(uid))
        elif isinstance(nd, And):
            fp.write(' "{}"[shape=invhouse, style= filled, fillcolor=grey, label="AND"];\n'.format(uid))
        elif isinstance(nd, Not):
            fp.write(' "{}"[shape=invtriangle, style= filled, fillcolor=grey, label="NOT"];\n'.format(uid))
        else:
            fp.write(' "{}"[style=filled, fillcolor=white, label="{}"];\n'.format(uid, nd))

        # add edges
        if isinstance(nd, Logic):
            for nnd in nd:
                uuid = gensym(nnd)
                fp.write(' "{}" -> "{}";\n'.format(uuid, uid))
                rec_visit(nnd)
        else:
            prev = dct.get(nd)
            if prev:
                uuid = gensym(prev)
                fp.write(' "{}" -> "{}";\n'.format(uuid, uid))
                rec_visit(prev)
            elif nd.startswith('true') and print_transitions:
                prev = nd.replace('true', 'next')
                uuid = gensym(prev)
                fp.write(' "{}" -> "{}" [style=dashed, color=blue];\n'.format(uuid, uid))
                #print(' "{}" -> "{}";'.format(uuid, uid))

    fp.write('digraph propNet\n{\n')

    for node in dct.keys():
        rec_visit(node)

    fp.write(' {rank=source; ' + '; '.join(gensym(n) for n in dct.keys() if n.startswith('does')) + ';}\n')
    fp.write(' {rank=source; ' + '; '.join(gensym(n) for n in dct.keys() if n.startswith('true')) + ';}\n')
    fp.write(' {rank=same; ' + '; '.join(gensym(n) for n in dct.keys() if n.startswith('next')) + ';}\n')
    fp.write(' {rank=same; ' + '; '.join(gensym(n) for n in dct.keys() if n.startswith('goal')) + ';}\n')
    fp.write(' {rank=same; ' + '; '.join(gensym(n) for n in dct.keys() if n.startswith('legal')) + ';}\n')
    fp.write(' {rank=same; ' + '; '.join(gensym(n) for n in dct.keys() if n.startswith('terminal')) + ';}\n')

    fp.write('}\n')
    fp.close()


init = ['true(cell(1,1,b))','true(cell(1,2,b))','true(cell(1,3,b))','true(cell(2,1,b))','true(cell(2,2,b))',
'true(cell(2,3,b))','true(cell(3,1,b))','true(cell(3,2,b))','true(cell(3,3,b))','true(control(white))']

init = ['true(cell(1,1,b))','true(cell(1,2,b))','true(cell(2,1,b))',
        'true(cell(2,2,b))','true(control(white))']


dct = foo('./rulesets/expanded/minittc.gdl')
#dct = foo('./rulesets/expanded-gdl/minittc.expanded')

propnet = PropNet(dct, init)
propnet.debug()

for actions in [
        ['does(white,mark(1,1))', 'does(black,noop)'],
        ['does(black,mark(2,2))', 'does(white,noop)'],
        ['does(white,mark(3,1))', 'does(black,noop)'],
        ['does(black,mark(2,2))', 'does(white,noop)'],
        ['does(white,mark(2,1))', 'does(black,noop)']
    ]:
    propnet.next_state(actions)
    propnet.debug()

#fp.close()
