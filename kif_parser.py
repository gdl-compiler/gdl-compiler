#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from functools import reduce

import ply.yacc as yacc

from kif import *

####################################################################


# Get the token map from the lexer.
from kif_lexer import tokens


def p_source(p):
    """
    source : empty
           | lsource 
    """
    p[0] = [] if not p[1] else p[1]

def p_lsource(p):
    """
    lsource : expr
            | lsource expr
    """
    expr = p[1] if len(p) == 2 else p[2]
    if isinstance(expr, Compound):
        if expr.name == '<=':
            expr = Rule(expr)
        else:
            expr = Fact(expr)
    if len(p) == 2:
        p[0] = [expr]
    elif len(p) == 3:
        p[0] = p[1] + [expr]


def p_expr(p):
    """
    expr : atom
         | list
    """
    p[0] = p[1]


def p_atom(p):
    """
    atom : variable
         | symbol
         | integer
         | IMPLIES
    """
    #print('atom', p)
    p[0] = p[1]

def p_list(p):
    """
    list : '(' seq ')'
    """
    p[0] = Compound(p[2])

def p_seq(p):
    """
    seq : expr
        | seq expr
    """
    if len(p) == 2:
        p[0] = [p[1]]
    else:
        p[0] = p[1] + [p[2]]

def p_empty(p):
    'empty :'
    pass

def p_symbol(p):
    'symbol : SYMBOL'
    p[0] = Symbol(p[1])

def p_number(p):
    'integer : INT'
    p[0] = Integer(p[1])
    # p[0] = p[1]
 
def p_variable(p):
    'variable : VAR'
    p[0] = Variable(p[1])


# Error rule for syntax errors
def p_error(p):
    raise RuntimeError('\tin ' + repr(p) + '\n\tSyntax error in input !')


# Build the parser
kif_parser = yacc.yacc()

