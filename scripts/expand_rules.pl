:- op(900, fy, ~).

~(Term) :- 
    ( \+ ground(Term) ->
      throw('Argl') ;
      true ). % hack : no use negation, simply ensure the term is already grounded;

% GDL builtin
distinct(X,Y) :- X \= Y.
or(X,Y) :- X ; Y.


% additional predicates
true(X) :- base(X).
does(X,Y) :- input(X,Y).


rewrite(Rule) :-
    ground(Rule), !.

rewrite(Rule) :-
    Rule =.. [:-,Head,Body],
    call(Body),
    ground(Rule).

writeln_dot(Term) :-
    write(Term), put_char("."), nl.
    
analyse_term(Term) :-
    forall(rewrite(Term), writeln_dot(Term)).
    
analyse_all(Filename) :-
    consult(Filename),
    read_file_to_terms(Filename,L,[]), % 'D:/gdl-compiler/rulesets/gdl/ttc.pl'
    string_concat(Filename, '.expanded', OutFilename),
    open(OutFilename, write, Fd), % use correct filename
    set_stream(Fd, alias(current_output)),
    maplist(analyse_term, L),
    close(Fd).
