:- op(900, fy, ~).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% reduce rules (on ne touche pas aux têtes de règles)
reduce((A :- true), A) :- !.
reduce((A :- B), A) :-         % si b est vrai, on transforme A en fait
    reduce(B, true), !.
reduce((A :- B), (A :- Pb)) :- !,
    reduce(B, Pb).

% reduce(true, true) :- !.
reduce(distinct(X, Y), true) :- !. % because GDL builtin have produced this term

reduce(or(X, Y), true) :-
    reduce(X, true), !.
reduce(or(X, Y), true) :-
    reduce(Y, true), !.
reduce(or(X, Y), or(ResX, ResY)) :- !,
    reduce(X, ResX),
    reduce(Y, ResY).

reduce((A, B), Res) :- !,
    reduce(A, Pa),
    reduce(B, Pb),
    combine(Pa, Pb, Res).

reduce(A, true) :-
    clause(A, true),
    \+ memberchk(A, [base(_), init(_), input(_,_)]), !. % don't touch these facts.
reduce(A, A).

combine(true, B, B):- !.
combine(A, true, A):- !.
combine(A, B, (A, B)).

/*
filter([Tx|Ts], Res) :-
    reduce(Tx, true), !,
    filter(Ts, Res).
filter([Tx|Ts], [NewTerm|Res]) :-
    reduce(Tx, NewTerm), !,
    filter(Ts, Res).
filter([],[]).
*/

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

deplie((A, B), [A|T]) :- !,
    deplie(B, T).
deplie(A, [A]).

replie([A], A) :- !.
replie([H|T], (H, C)) :- !,
    replie(T, C).

order_term(Head :- Body, Head :- NewBody) :- !,
    deplie(Body, Clauses),
    sort(Clauses, SortedCl),
    replie(SortedCl, NewBody).
order_term(A, A).

order_all([H1|T1], [H2|T2]) :-
    order_term(H1, H2),
    order_all(T1, T2).
order_all([], []).
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
rewrite_term(Term) :-
    reduce(Term, NewTerm),
    ( NewTerm = true ->
      true ;
      write(NewTerm), put_char("."), nl ).

analyse_all(Filename) :-
    consult(Filename),
    read_file_to_terms(Filename, Prog, []),
    order_all(Prog, OProg),
    sort(OProg, SProg),
    string_concat(Filename, '.filt', OutFilename),
    open(OutFilename, write, Fd), % use correct filename
    set_stream(Fd, alias(current_output)),
    maplist(rewrite_term, SProg),
    close(Fd).
