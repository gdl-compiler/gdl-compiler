#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
from pprint import pprint

from kif_lexer import kif_lexer
from kif_parser import kif_parser
from kif import gdl_objs


if __name__ == '__main__':
    src = sys.argv[1]
    with open(src, 'r') as fp:
        prgm = fp.read()
        ret = kif_parser.parse(prgm, lexer=kif_lexer)
        for e in ret:
            print(e.to_gdl(), '\n'); # input('continuer ?')
        pprint(gdl_objs)
        #print(gdl_objs3)

