#!/usr/bin/env python3
# -*- coding: utf-8 -*-


import ply.yacc as yacc


# Get the token map from the lexer.
from gdl_lexer import tokens
from logic import Not


class Rule:
    def __init__(self, head, body):
        self.head = head
        self.body = body
    def __repr__(self):
        return repr(self.head) + ':-' + ','.join(repr(clause) for clause in self.body) + '.'

class Fact:
    def __init__(self, head):
        self.head = head
    def __repr__(self):
        return repr(self.head) + '.'

class Compound:
    def __new__(cls, functor, args):
        if functor == 'not':
            return Not(args)
        return super().__new__(cls)
    def __init__(self, functor, args):
        if functor == 'base':
            self.functor = Atom('true')
        elif functor == 'input':
            self.functor = Atom('does')
        else:
            self.functor = functor
        self.args = args
    def __repr__(self):
        return self.functor + '(' + ','.join(repr(arg) for arg in self.args) + ')'

class Atom(str):
    def __repr__(self):
        return self


def p_source(p):
    """
    source : empty
           | lsource 
    """
    p[0] = [] if not p[1] else p[1]

def p_lsource(p):
    """
    lsource : term
            | lsource term
    """
    if len(p) == 2:
        p[0] = [p[1]]
    else:
        p[0] = p[1] + [p[2]]

def p_term(p):
    """
    term : rule
         | fact
    """
    p[0] = p[1]

def p_expr(p):
    """
    expr : atom
         | compound
    """
    p[0] = p[1]

def p_compound(p):
    """
    compound : atom '(' seq ')'
    """
    p[0] = Compound(p[1], p[3])

def p_atom(p):
    """
    atom : ATOM
    """
    p[0] = Atom(p[1])

def p_fact(p):
    """
    fact : expr '.'
    """
    p[0] = Fact(p[1])

def p_rule(p):
    """
    rule : expr IMPLIES seq '.'
    """
    head = p[1]
    #if isinstance(head, Atom): # hack
    #    head.functor = head
    p[0] = Rule(p[1], p[3])

def p_seq(p):
    """
    seq : expr
        | seq ',' expr
    """
    if len(p) == 2:
        p[0] = [p[1]]
    else:
        p[0] = p[1] + [p[3]]

def p_empty(p):
    'empty :'
    pass

# Error rule for syntax errors
def p_error(p):
    raise RuntimeError('\tin ' + repr(p) + '\n\tSyntax error in input !')


# Build the parser
gdl_parser = yacc.yacc()

