#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import os

from kif_lexer import kif_lexer
from kif_parser import kif_parser
from kif import *

def test(src_dir, filename):
    in_filepath = src_dir + '/' + filename
    ou_filepath = src_dir + '/' + filename.replace('.kif', '.pl')
    with open(in_filepath, 'r') as fp, open(ou_filepath, 'w') as ofp:
        prgm = fp.read()
        try:
            ret = kif_parser.parse(prgm, lexer=kif_lexer)
        except Exception as err:
            print('\tERROR on', in_filepath, file=sys.stderr)

        for term in ret:
            print(term.to_gdl(), file=ofp)

    
if __name__ == '__main__':
    src_dir = sys.argv[1]
    for filename in os.listdir(src_dir):
        if filename.endswith('.kif'):
            test(src_dir, filename)
